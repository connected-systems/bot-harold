"""simple_controller controller."""
from controller import Supervisor
import random

import client

robot = Supervisor()
supervisorNode = robot.getSelf()

numberOfSensors = 4
distanceSensors = []
distanceSensorsNames = ['ds_north', 'ds_east', 'ds_south', 'ds_west']
# get the time step of the current world.
timestep = int(robot.getBasicTimeStep())
duration = (1000 // timestep) * timestep
botPos = [12, 12]  # Y AND X
flagPos = [0, 0]  # Y AND X
flagKnown = False

map = [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
       [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]]

[[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
 [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
 [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1],
 [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
 [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
 [1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
 [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
 [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
 [0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1],
 [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1],
 [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1],
 [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1]]

def initialize():
    print("Initialization...")

    for i in range(numberOfSensors):
        distanceSensors.append(robot.getDistanceSensor(distanceSensorsNames[i]))
        distanceSensors[i].enable(timestep)


def check_walkable():
    walkable = []
    for i in range(numberOfSensors):
        if i == 0 and map[botPos[0] - 1][botPos[1]] == 0:
            walkable.append(i)
        elif i == 1 and map[botPos[0]][botPos[1] + 1] == 0:
            walkable.append(i)
        elif i == 2 and map[botPos[0] + 1][botPos[1]] == 0:
            walkable.append(i)
        elif i == 3 and map[botPos[0]][botPos[1] - 1] == 0:
            walkable.append(i)

    print("walkable = ", walkable)
    return walkable


def move_random():
    direction = random.choice(check_walkable())
    print("direction = ", direction)
    pos = supervisorNode.getPosition()
    trans = supervisorNode.getField("translation")
    if direction == 0:
        pos[2] -= 0.1  # Move North
        botPos[0] -= 1
    elif direction == 1:
        pos[0] += 0.1  # Move East
        botPos[1] += 1
    elif direction == 2:
        pos[2] += 0.1  # Move South
        botPos[0] += 1
    elif direction == 3:
        pos[0] -= 0.1  # Move West
        botPos[1] -= 1

    trans.setSFVec3f(pos)


def scan():
    """returns a scan of the surroundings of the bots in a list of tuples (coordinate, obstacle)"""
    for i in range(numberOfSensors):
        if i == 0:
            print("north = ", distanceSensors[i].getValue())
            if distanceSensors[i].getValue() < 200:
                map[botPos[0] - 1][botPos[1]] = 1
            if 500 < distanceSensors[i].getValue() > 600:
                flagPos[0] = botPos[0] - 1
                flagPos[1] = botPos[1]
        elif i == 1:
            print("east = ", distanceSensors[i].getValue())
            if distanceSensors[i].getValue() < 200:
                map[botPos[0]][botPos[1] + 1] = 1
            if 500 < distanceSensors[i].getValue() > 600:
                flagPos[0] = botPos[0]
                flagPos[1] = botPos[1] + 1
        elif i == 2:
            print("south = ", distanceSensors[i].getValue())
            if distanceSensors[i].getValue() < 200:
                map[botPos[0] + 1][botPos[1]] = 1
            if 500 < distanceSensors[i].getValue() > 600:
                flagPos[0] = botPos[0] + 1
                flagPos[1] = botPos[1]
        elif i == 3:
            print("west = ", distanceSensors[i].getValue())
            if distanceSensors[i].getValue() < 200:
                map[botPos[0]][botPos[1] - 1] = 1
            if 500 < distanceSensors[i].getValue() > 600:
                flagPos[0] = botPos[0]
                flagPos[1] = botPos[1] - 1


def send(message):
    """Sends message to server"""
    client.send(message)

def receive():
    """retrieves information from server"""
    client.receive()

def update(newmap):
    """sets updated flag position, obstacles and bot positions"""

initialize()
while robot.step(duration) != -1:
    scan()

    move_random()
    print(map)

    pass




def checkWalkable():
    walkable = []
    for i in range(0, 3):
        if (i == 0 and map[botPos[0] - 1][botPos[1]] == 0):
            walkable.append(i)
        elif i == 1 and map[botPos[0]][botPos[1] + 1] == 0:
            walkable.append(i)
        elif i == 2 and map[botPos[0] + 1][botPos[1]] == 0:
            walkable.append(i)
        elif i == 3 and map[botPos[0]][botPos[1] - 1 == 0:
            walkable.append(i)

    return walkable

def move_astar():
    pass


while robot.step(duration) != -1:

    scan()
    send()
    receive()
    update()

    if flag_pos is None:
        move_random()
    else:
        move_astar()




