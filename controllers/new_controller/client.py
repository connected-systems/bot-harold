import socket
import json
import sys
import pickle


class Client:

    def __init__(self, host="40.114.208.88", port=1024, id=0):
        self._HOST = host
        self._PORT = port
        self._ID = id
        self._HEADERSIZE = 10
        self._VALID = False
        # Create a socket (SOCK_STREAM means a TCP socket)
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # Connect to server and send data
            self.client.connect((self._HOST, self._PORT))
            self.client.setblocking(True)
        except:
            print("Error on connecting")

    def send(self, object):

        # user_encode_data = json.dumps(object, indent=2).encode('utf-8')  
        # self.client.connection.sendall(bytes(message, "utf-8"))   
        message = json.dumps(object)
        self.client.sendall(bytes(message, "utf-8"))
        # print("Sent: ", object)

    def receive(self):
        # print(self.client.recv(1056))
        # unpickled = pickle.loads(self.client.recv(1056))
        self._VALID = False

        received = self.client.recv(1024)
        print("received: ", received)
        unloaded = None
        print(received)
        try:
            unloaded = json.loads(received)
            # print("json: ",unloaded)
            self._VALID = True
            return unloaded
        except:
            print("RECEIVED --------------")
            print(unloaded)
            print("Something other than json received")

        return None
