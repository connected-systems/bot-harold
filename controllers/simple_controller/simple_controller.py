"""simple_controller controller."""
from controller import Supervisor
import random
import json
from client import Client
import node

robot = Supervisor()
supervisorNode = robot.getSelf()

numberOfSensors = 4
numberOfSimulations = 2
distanceSensors = []
distanceSensorsNames = ['ds_north', 'ds_east', 'ds_south', 'ds_west']
leds = []
ledsNames = ['led0', 'led1', 'led2', 'led3']
simulatedRobots = []
startingPosition = [[0.0, 0.05, 0.0], [1.1, 0.05, 0.0], [1.1, 0.05, 1.1], [0.0, 0.05, 1.1]]

# get the time step of the current world.
timestep = int(robot.getBasicTimeStep()) + 500
duration = (1000 // timestep) * timestep

botPos = [12, 12] # Y AND X, starts at 0, 0
flagPos = [0, 0] # Y AND X
flagKnown = False
direction = None
client = Client()

map  = [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]]

jsonObject = {
    "Sender": 2,
    "Robots": [],
    "Gamemap":
        {
            "map": map,
            "flag": [0.0, 0.0]
        }
}


def initialize():
    print("Initialization...")

    for i in range(numberOfSensors):
        distanceSensors.append(robot.getDistanceSensor(distanceSensorsNames[i]))
        distanceSensors[i].enable(timestep)
        leds.append(robot.getLED(ledsNames[i]))

    # append robots to json and initialize simulated robots
    for i in range(0, 4, 1):
        jsonObject["Robots"].append({"id": i, "location": startingPosition[i], "direction": 0})

    for i in range(numberOfSimulations):
        simulatedRobots.append(robot.getFromDef("Sim{0}".format(i)))

    jsonObject["Gamemap"]["map"] = map
    jsonObject["Gamemap"]["flag"] = [0.0, 0.0]


def leds_off():
    for led in range(numberOfSensors):
        leds[led].set(0)


def updateMap():
    global flagKnown
    package = client.receive()

    if package is None or package["Sender"] == 2:
        return False
    else:
        print("RECEIVED THE FOLLOWING")
        print(package["Robots"][2]["location"])
        map = package["Gamemap"]["map"]
        # print("flag from package is = ", package["Gamemap"]["flag"])
        for sim in range(numberOfSimulations):
            # print(sim)
            trans_field = simulatedRobots[sim].getField("translation")
            # print("ROBOT", sim, " LOCATION", package["Robots"][sim + 1]["location"])
            jsonObject["Robots"][sim]["location"] = simulatedRobots[sim].getField("translation").getSFVec3f()
            # print(package["Robots"][sim]["location"])
            trans_field.setSFVec3f(package["Robots"][sim]["location"])
        if package["Gamemap"]["flag"] == [0.0, 0.0]:
            # print("default flag position, continuing")
            pass
        else:
            # print("received flag position i think")
            flagPos[1] = package["Gamemap"]["flag"][0] * 10 + 1
            flagPos[0] = package["Gamemap"]["flag"][1] * 10 + 1
            flagKnown = True

    return True





def check_walkable():
    walkable = []
    for i in range(numberOfSensors):
        if i == 0 and map[botPos[0]-1][botPos[1]] == 0:
            walkable.append(i)
        elif i == 1 and map[botPos[0]][botPos[1]+1] == 0:
            walkable.append(i)
        elif i == 2 and map[botPos[0]+1][botPos[1]] == 0:
            walkable.append(i)
        elif i == 3 and map[botPos[0]][botPos[1]-1] == 0:
            walkable.append(i)

    print("walkable = ", walkable)
    return walkable


def move():
    leds_off()
    global direction
    print("direction = ", direction)
    pos = supervisorNode.getPosition()
    trans = supervisorNode.getField("translation")
    if direction == 0:
        leds[direction].set(1)
        pos[2] -= 0.1       # Move North
        botPos[0] -= 1
    elif direction == 1:
        leds[direction].set(1)
        pos[0] += 0.1       # Move East
        botPos[1] += 1
    elif direction == 2:
        leds[direction].set(1)
        pos[2] += 0.1       # Move South
        botPos[0] += 1
    elif direction == 3:
        leds[direction].set(1)
        pos[0] -= 0.1       # Move West
        botPos[1] -=1

    trans.setSFVec3f(pos)
    # print("pos after moving is ", pos)

    jsonObject["Robots"][client._ID]["location"] = [round(pos[0],2), round(pos[1],2), round(pos[2],2)]
    jsonObject["Robots"][client._ID]["direction"] = direction


def move_random():
    global direction
    direction = random.choice(check_walkable())
    move()


def send():
    """sends information to server"""
    pos = supervisorNode.getPosition()
    jsonObject["Gamemap"]["map"] = map
    jsonObject["Robots"][client._ID]["location"] = [round(pos[0],2), round(pos[1],2), round(pos[2],2)]
    # if flagPos is not [0, 0]:
    #     jsonObject["Gamemap"]["flag"][0] = (flagPos[1]-1)/10
    #     jsonObject["Gamemap"]["flag"][1] = (flagPos[0]-1)/10

    client.send(jsonObject)
    print("SENT THE FOLLOWING")
    print(jsonObject["Robots"][2]["location"])


def move_astar():
    """uses the node class to calculate a route and move using astar"""
    global direction
    print("now doing the astar thing yaknow bud", flagPos)
    path = node.astar(map, tuple(botPos), tuple(flagPos))
    print(path)
    nextstep = list(path[1])
    if botPos[0] - nextstep[0] == 1:
        direction = 0
    elif botPos[1] - nextstep[1] == -1:
        direction = 1
    elif botPos[0] - nextstep[0] == -1:
        direction = 2
    elif botPos[1] - nextstep[1] == 1:
        direction = 3
    else: print("INVALID STEP SIZE OR DIAGONAL STEP")
    move()


def scan():
    """returns a scan of the surroundings of the bots in a list of tuples (coordinate, obstacle)"""
    global flagKnown
    # print("running scan")
    for i in range(numberOfSensors):
        if i == 0:
            # print("north = ", distanceSensors[i].getValue())
            if distanceSensors[i].getValue() < 200:
                map[botPos[0]-1][botPos[1]] = 1
            elif 500 <= distanceSensors[i].getValue() <= 600:
                print("found the flag!")
                flagKnown = True
                flagPos[0] = botPos[0]-1
                flagPos[1] = botPos[1]
            else: continue
        elif i == 1:
            # print("east = ", distanceSensors[i].getValue())
            if distanceSensors[i].getValue() < 200:
                map[botPos[0]][botPos[1]+1] = 1
            elif 500 <= distanceSensors[i].getValue() <= 600:
                print("found the flag!")
                flagKnown = True
                flagPos[0] = botPos[0]
                flagPos[1] = botPos[1]+1
            else: continue
        elif i == 2:
            # print("south = ", distanceSensors[i].getValue())
            if distanceSensors[i].getValue() < 200:
                map[botPos[0]+1][botPos[1]] = 1
            elif 500 <= distanceSensors[i].getValue() <= 600:
                print("found the flag!")
                flagKnown = True
                flagPos[0] = botPos[0]+1
                flagPos[1] = botPos[1]
            else: continue
        elif i == 3:
            # print("west = ", distanceSensors[i].getValue())
            if distanceSensors[i].getValue() < 200:
                map[botPos[0]][botPos[1]-1] = 1
            elif 500 <= distanceSensors[i].getValue() <= 600:
                print("found the flag!")
                flagKnown = True
                flagPos[0] = botPos[0]
                flagPos[1] = botPos[1]-1
        else: break


initialize()
send()
while robot.step(duration) != -1:


    scan()
    # receive first to be sure you have the latest map
    if updateMap():
        send()
        if flagPos == botPos:
            print("ladies and gentlemen, we got him")
        elif flagKnown:
            move_astar()
        else:
            move_random()
    else:
        print("no messages yet")
        pass




    pass
