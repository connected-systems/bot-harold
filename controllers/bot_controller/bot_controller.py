from controller import Supervisor
import time
import random
from node import Node
from gameState import GameState
import heapq
from path import Path
from client import Client
import pickle
import json

multiplayer = True

translationXAxis = 1
translationZAxis = 1

stepdistanceX = 0.1
stepdistanceZ = 0.1
# TO DO:
# - debug position in node. make sure every node gets a position if found by robot
# - a star algorithm
# - server implementation

# create the Robot instance.
robot = Supervisor()
simulatedRobots = []

supervisorNode = robot.getSelf()

# get the time step of the current world.
timestep = int(robot.getBasicTimeStep()) + 4000

duration = (1000 // timestep) * timestep

temporaryRandom = 0
quitGame = False

numberOfSimulations = 3
numberOfSensors = 4
distanceSensors = []
distanceSensorsNames = ['ds_front', 'ds_back', 'ds_left', 'ds_right']
leds = []
ledsNames = ['ledF', 'ledB', 'ledL', 'ledR']

flagNode = Node()
startingNode = Node()
rows = 12
columns = 12
path = []

start = (0, 0)
flag = (0.0, 0.9)

game_map = [[0 for x in range(columns)] for y in range(rows)]

maze = [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]]

jsonObject = {
    "Robots": [
        {
            "id": robot.getName(),
            "location": [0.0, 0.0, 0.0],
            "direction": 0
        }
    ],
    "Gamemap":
        {
            "map": maze,
            "flag": [1.0, 0.0, 1.0]
        }
}


def initialize():
    print("Initialization...")
    for i in range(0, 4, 1):
        jsonObject["Robots"].append({"id": i, "location": [0.0, 0.0], "direction": 0})

    jsonObject["Gamemap"]["map"] = maze
    jsonObject["Gamemap"]["flag"] = [1.0, 1.0]

    # initialize map
    for i in range(rows):
        for j in range(columns):
            node = Node()
            game_map[i][j] = node

    # initialize sensors
    for i in range(numberOfSensors):
        distanceSensors.append(robot.getDistanceSensor(distanceSensorsNames[i]))
        distanceSensors[i].enable(timestep)
        leds.append(robot.getLED(ledsNames[i]))

    # initialize simulated robots
    for sim in range(0, numberOfSimulations):
        simulatedRobots.append(robot.getFromDef("R{0}".format(sim)))
        trans_field = simulatedRobots[sim].getField("translation")
        values = trans_field.getSFVec3f()
        print("ROBOTSIM POSITION: ", values[0], values[2])
        jsonObject["Robots"][sim + 1]["location"] = [values[0], values[1], values[2]]


def printMap():
    print("------------------------------------")

    for i in range(rows - 1, -1, -1):
        for j in range(columns):
            print(game_map[i][j].getName(), end=' ')
        print('')

    print("------------------------------------")

    for i in range(rows - 1, -1, -1):
        for j in range(columns):
            print(jsonObject["Gamemap"]["map"][i][j], end=' ')
        print('')

    print("------------------------------------")


def printPositions():
    print("------------------------------------")

    for i in range(rows - 1, -1, -1):
        for j in range(columns):
            print(game_map[i][j].getPosition(), end=' ')
        print('')

    print("------------------------------------")


def isTargetFound(posX, posZ):
    # no need to set a step if we already reached the target so we first check for that
    if (posX == flag[0] and posZ == flag[1]):
        print("Target reached...")
        return True


# Calculate h (heuristic)
def heuristic(node, goal):
    (x1, y1) = node.position
    # print(goal)
    (x2, y2) = goal.position
    # Manhattan calculation
    return abs(x1 - x2) + abs(y1 - y2)


# Check if a position is inside the board
def positionInsidegame_map(game_map, x, y):
    # print("positions inside gamemap = ", (x, y), end = ' ')
    if (x >= 0 and y >= 0 and y < 1.0 and x < 1.0):
        # print("position is in gamemap")
        return True

    # print("position not in gamemap")
    return False


# Check if new node position is walkable
def isWalkable(game_map, node):
    (x, y) = node.position

    # print("node position in tuple", node.position)

    if (game_map[int(x * 10) + translationXAxis][int(y * 10 + translationZAxis)].getName() == "X" or
            game_map[int(x * 10) + translationXAxis][int(y * 10) + translationZAxis].getName() == "0"):
        # print("node walkable is false")
        return False

    # print("node walkable is true")
    return True


# Visualize the solution graphically
def getQuickestRoute(array, node):
    (x, y) = node.position

    route = []
    # Draw the solution. Starting at the goal node
    while node:
        (x, y) = node.position
        route.append(node.position)
        # print("NODE POSITION: ", x, y)
        game_map[int(x * 10) + translationXAxis][int(y * 10) + 1].setName("*")
        # Set current nodes parent as next node to draw
        node = node.parent

    # printMap()
    return route


def a_star(game_map, start, goal):
    fringeList = []  # The open list containing all nodes that are to be visited
    # Using a heap to always have the node with lowest f value at index 0
    closedList = []  # Contains all visited nodes

    # print("COordinates of start: ", start)
    # print("COordinates of goal: ", goal[0] * 10 + translationXAxis," ",  goal[1] * 10 + translationXAxis)
    # print(" in gamemap: ", game_map[int(start[0] * 10)+ translationXAxis][int(start[1] * 10)+ translationXAxis])
    # print(" goal in gamemap" ,game_map[int(goal[0] * 10) + translationXAxis][int(goal[1] * 10) + translationXAxis] )
    startNode = game_map[int(start[0] * 10) + translationXAxis][int(start[1] * 10) + translationZAxis]
    goalNode = game_map[int(goal[0] * 10) + translationXAxis][int(goal[1] * 10) + translationXAxis]

    # print(goalNode)
    startNode.h = heuristic(startNode, goalNode)

    # Pushing the startnode into the heap    
    heapq.heappush(fringeList, startNode)

    # Right, bottom, left, top
    neighbors = [(0.1, 0), (0, 0.1), (-0.1, 0), (0, -0.1)]

    while fringeList:
        # printMap()

        # Get the node at index 0 in fringeList, which has the lowest f value
        current = heapq.heappop(fringeList)
        closedList.append(current)

        # Found solution. Draw it by using function above
        if (current == goalNode):
            # print("-----NODE----")
            # print("nodes position", current.getPosition())
            # print("nodes name",current.getName())
            # print("nodes state",current.getState())
            # print("-------------")
            # print("SOLUTION FOUND")
            return getQuickestRoute(game_map, current)

        # print("Current is of type or value ", current, " and goal of value ", goal)

        keyPressed = 0
        # For each possible neighbor
        for neighbor in neighbors:

            (x, y) = current.position
            # print("Current position: ", (x, y))
            # Using values from the neighbors game_map to find neighbors in the 4 directions
            x += neighbor[0]
            y += neighbor[1]

            x = round(x, 1)
            y = round(y, 1)

            # print("x and y position of neighbour: ", (x, y))

            # Checking if neighbors position is possible
            if (positionInsidegame_map(game_map, x, y) == False):
                # print("neighbours position is not possible", x, y)
                continue

            neighborNode = game_map[int(x * 10) + translationXAxis][int(y * 10) + 1]

            if neighborNode.position is None:
                # print(neighborNode)
                # print(game_map[int((x * 10) + translationXAxis)][int((y * 10) + translationXAxis)])
                # print("Neebournode: ", neighborNode)
                continue

            # If neighbor is already visited, skip to next neighbor
            if neighborNode in closedList:
                # print("neight in closedList: ", neighborNode)
                continue
            # Set the g cost to current nodes value + the cost of the neighbor
            g = current.g + neighborNode.cost

            # If node isn't already in fringeList, add it to fringeList
            # print("-----NEIGHBOUR----")
            # print("neighbournode position", neighborNode.getPosition())
            # print("neighbournode name",neighborNode.getName())
            # print("neighbournode state",neighborNode.getState())
            # print("-------------")
            if neighborNode not in fringeList and isWalkable(game_map, neighborNode):
                # print(" PPPPPPP that is added to fringe: ", neighborNode)       
                neighborNode.g = g
                neighborNode.h = heuristic(neighborNode, goalNode)
                neighborNode.parent = current
                heapq.heappush(fringeList, neighborNode)
            # If current nodes g value is higher than the neighbors, skip to next neighbor
            elif g >= neighborNode.g:
                # print("Current node g high than neighbors")
                continue

            # Update neighors g, h and parent
            neighborNode.g = g
            neighborNode.h = heuristic(neighborNode, goalNode)
            neighborNode.parent = current

    if (not fringeList):
        print('No solution')
        return True


def findObstacle(posX, posZ, gamemap_row, gamemap_column):
    # look for obstacles or the wall
    for i in range(numberOfSensors):
        # print(i, "\'s name = ", distanceSensors[i].getName(), " value = ",distanceSensors[i].getValue())
        if (distanceSensors[i].getValue() < 200):
            if (i == 0 and game_map[int((posX + stepdistanceX) * 10) + translationXAxis][
                int((posZ * 10) + translationZAxis)].getState() == False):
                gamemap_row = int((posX + stepdistanceX) * 10 + translationXAxis)
                gamemap_column = int((posZ * 10) + translationZAxis)

            elif (i == 1 and game_map[int((posX - stepdistanceX) * 10) + translationXAxis][
                int(posZ * 10) + translationZAxis].getState() == False):
                gamemap_row = int((posX - stepdistanceX) * 10) + translationXAxis
                gamemap_column = int(posZ * 10) + translationZAxis

            elif (i == 2 and game_map[int(posX * 10) + translationXAxis][
                int((posZ - stepdistanceZ) * 10) + translationZAxis].getState() == False):
                gamemap_row = int((posX * 10) + translationXAxis)
                gamemap_column = int((posZ - stepdistanceZ) * 10) + translationZAxis

            elif (i == 3 and game_map[int((posX * 10) + translationXAxis)][
                int((posZ + stepdistanceZ) * 10) + translationZAxis].getState() == False):
                gamemap_row = int((posX * 10) + translationXAxis)
                gamemap_column = int((posZ + stepdistanceZ) * 10 + translationZAxis)

            updateNodePosition(i, posX, posZ, gamemap_row, gamemap_column)
            game_map[gamemap_row][gamemap_column].setName("X")
            game_map[gamemap_row][gamemap_column].setState(True)
            jsonObject["Gamemap"]["map"][gamemap_row][gamemap_column] = "X"


def updateNodePosition(direction, posX, posZ, gamemap_row, gamemap_column):
    if direction == 0:
        game_map[gamemap_row][gamemap_column].setPosition((posX + stepdistanceX, posZ))


    elif direction == 1:
        game_map[gamemap_row][gamemap_column].setPosition((posX - stepdistanceX, posZ))


    elif direction == 2:
        game_map[gamemap_row][gamemap_column].setPosition((posX, posZ - stepdistanceZ))

    elif direction == 3:
        game_map[gamemap_row][gamemap_column].setPosition((posX, posZ + stepdistanceZ))


def randomWalk(posX, posZ, direction, translationOfRobot):
    if distanceSensors[direction].getValue() > 200:
        if direction == 0:
            # pass
            leds[0].set(1)
            posX += stepdistanceX

        elif direction == 1:
            # pass
            leds[1].set(1)
            posX -= stepdistanceX

        elif direction == 2:
            # pass
            leds[2].set(1)
            posZ -= stepdistanceZ

        elif direction == 3:
            leds[3].set(1)
            posZ += stepdistanceZ

        jsonObject["Robots"][client._ID]["location"] = [posX, 0.0, posZ]
        jsonObject["Robots"][client._ID]["direction"] = direction
        setStep(posX, posZ, translationOfRobot)


def setStep(posX, posZ, translationOfRobot):
    # printMap()  
    translationOfRobot.setSFVec3f([posX, 0, posZ])


def turnOffLed():
    for led in range(numberOfSensors):
        leds[led].set(0)


def sendInformationToServer(message):
    client.send(message)


def updateMap(package):
    for r in range(0, rows, 1):
        for c in range(0, 1, columns):
            game_map[r][c].setName(package["Gamemap"]["map"][r][c])

    for sim in range(numberOfSimulations):
        trans_field = simulatedRobots[sim].getField("translation")
        print("SIM PLUS 1: ", sim + 1)
        # print("ROBOT", sim, " LOCATION", package["Robots"][sim + 1]["location"])
        print(package["Robots"][sim + 1]["location"])
        trans_field.setSFVec3f(package["Robots"][sim + 1]["location"])




# main program:
def main(path, temporaryRandom, gameState, client, jsonObject):
    if multiplayer:
        sendInformationToServer(jsonObject)
        received = client.receive()
        print(received)

    if received is not None:
        updateMap(received)

    # turn off all leds
    turnOffLed()
    # get the robots position
    robotPosition = supervisorNode.getPosition()
    posX = round(robotPosition[0], 2)  # times 10 because grid size is 0.1 x 0.1 m
    posZ = round(robotPosition[2], 2)
    translationOfRobot = supervisorNode.getField("translation")

    leds[temporaryRandom].set(0)
    direction = random.randint(0, 3)
    temporaryRandom = direction

    if isTargetFound(posX, posZ) and jsonObject["Gamemap"]["flag"] == [1.0, 1.0]:
        # print("flag found on position: ", posX, posZ)
        gameState.targetFound = True
        translationOfRobot.setSFVec3f([start[0], 0, start[1]])
        game_map[int((flag[0] * 10) + translationXAxis)][int((flag[1] * 10) + translationZAxis)].setName("F")
        jsonObject["Gamemap"]["map"][int((flag[0] * 10) + translationXAxis)][
            int((flag[1] * 10) + translationXAxis)] = "F"
        jsonObject["Gamemap"]["flag"] = [posX, posZ]
        game_map[int((flag[0] * 10) + translationXAxis)][int((flag[1] * 10) + translationZAxis)].setState(True)
        game_map[int((flag[0] * 10) + translationXAxis)][int((flag[1] * 10) + translationZAxis)].setPosition(
            (round(flag[0], 1), round(flag[1], 1)))
        printMap()

        # print("target found on position: ", game_map[int((flag[0] * 10) + translationXAxis)][int((flag[1] * 10) + translationZAxis)].getName())

    # Robots position is not an obstacle or flag
    if (game_map[int((posX * 10) + translationXAxis)][int((posZ * 10) + translationZAxis)].getState() == False):
        game_map[int((posX * 10) + translationXAxis)][int((posZ * 10) + translationZAxis)].setName(" ")
        jsonObject["Gamemap"]["map"][int((posX * 10) + translationXAxis)][int((posZ * 10) + translationXAxis)] = " "
        game_map[int((posX * 10) + translationXAxis)][int((posZ * 10) + translationZAxis)].setState(True)
        # print("updating position with x and y: ", posX, " ", posZ)
        game_map[int((posX * 10) + translationXAxis)][int((posZ * 10) + translationZAxis)].setPosition(
            (round(posX, 1), round(posZ, 1)))

    if gameState.targetFound == False:
        # print current map
        # printMap()
        randomWalk(posX, posZ, direction, translationOfRobot)
    elif gameState.pathFound == False:

        # print("FINDING PATH")
        path.setPath(a_star(game_map, start, flag))
        path.sortNodes()
        gameState.pathFound = True
    elif gameState.pathFound == True:
        # print("Steps to go: ", len(path.getPath()))
        if len(path.getPath()) == 0:
            while 1:
                pass
            # return True
        try:
            (posX, posZ) = path.getNode()
        except:
            pass
        setStep(posX, posZ, translationOfRobot)
        try:
            path.popNode(0)
        except:
            pass

    elif gameState.quitGame == True:
        return True

    gamemap_row = 0
    gamemap_column = 0
    findObstacle(posX, posZ, gamemap_row, gamemap_column)

    # client.receive()
    # message = "position x: {0} s".format(posX)
    # sendInformationToServer(message)
    # if multiplayer:
    # print(jsonObject)
    # sendInformationToServer(jsonObject)
    # sendInformationToServer("message")
    # client.receive()

    return False


gameState = GameState()
initialize()
startingNode.position = (0, 0)
flagNode.position = (0.0, 0.2)
path = Path()
client = Client(host="40.85.74.188", port=1024)

print("Running...")
while robot.step(timestep) != -1 and quitGame == False:
    quitGame = main(path, temporaryRandom, gameState, client, jsonObject)
